var checkIfExists = (item) => {
  if (!item) {
    throw new Error(404);
  } else {
    return item;
  }
};

module.exports = {
  checkIfExists: checkIfExists
};
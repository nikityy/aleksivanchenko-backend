var Sequelize = require('sequelize');
var Promise   = require('bluebird');
var Photo     = require('./photo');
var yaml      = require('js-yaml');
var fs        = Promise.promisifyAll(require('fs'));
var database  = require('./database');

var Portfolio = database.define('portfolio', {

  title: {
    type: Sequelize.STRING,
    allowNull: false,
  },

  description: {
    type: Sequelize.STRING,
    allowNull: true,
  },

  tagline: {
    type: Sequelize.STRING
  },

  url: {
    type: Sequelize.STRING,
    allowNull: false,
  },

  size: {
    type: Sequelize.ENUM('normal', 'big'),
    allowNull: false,
  },

  textColor: {
    type: Sequelize.ENUM('black', 'white'),
    allowNull: false,
  },

  image: {
    type: Sequelize.STRING,
    allowNull: false,
  },

  sort: {
    type: Sequelize.INTEGER,
    allowNull: false,
  }

}, {
  classMethods: {

    forceFulfillFromDirectory(directory) {
      return Promise.all([
        Photo.truncate(),
        Portfolio.truncate()
      ]).then(() => Portfolio.fulfillFromDirectory(directory))
    },

    fulfillFromDirectory(directory) {
      var getSubdirectoryPath = (subdir) => directory + '/' + subdir;
      var getMetaFilePath = (subdir) => subdir + '/meta.yaml';
      var isDirectory = (dir) => fs.statAsync(dir).then((stat) => stat.isDirectory());
      var isFile = (file) => fs.statAsync(file).then((stat) => stat.isFile());
      var readFile = (filePath) => fs.readFileAsync(filePath, 'utf8');
      var readDirectory = (dir) => fs.readdirAsync(dir);
      var isImage = (name) => name.substr(-3) === 'jpg' || name.substr(-4) === 'jpeg';
      var classificateImage = (name) => {
        var match = /\d+--(\D*)\.\D+/.exec(name);

        if (match) {
          return match[1];
        } else {
          return 'normal';
        }
      };

      return fs.readdirAsync(directory)
        .map(getSubdirectoryPath)
        .filter(isDirectory)
        .map(getMetaFilePath)
        .filter(isFile)
        .map(readFile)
        .map(yaml.safeLoad)
        .each((hash) => {
          var photosDirPath = getSubdirectoryPath(hash.url) + '/photos';

          return isDirectory(photosDirPath).then((isDirectory) => {
            if (isDirectory) {
              return readDirectory(photosDirPath)
              .filter(isImage)
              .map((name) => ({ url: name, size: classificateImage(name)}))
              .map((image) => Photo.create(image))
              .then((photos) => {
                try {
                  return Portfolio.create(hash).then((portfolio) => portfolio.setPhotos(photos));
                }
                catch(e) {
                  console.error(e);
                }
              });
            }
          });
        });
    }

  }
});

Portfolio.hasMany(Photo, { as: 'photos' });
Portfolio.sync();

module.exports = Portfolio;

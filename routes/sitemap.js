var express = require('express');
var sitemapRouter = express.Router();
var expressSitemap = require('express-sitemap');

var sitemap = expressSitemap({
  url: 'weds.by',
  sitemapSubmission: '/sitemap.xml',
  map: {
    '/blog': [ 'get' ],
    '/categories': [ 'get' ],
    '/portfolio': [ 'get' ]
  },
  route: {
    '/blog': {
        changefreq: 'daily',
        priority: 0.7,
    },
    '/categories': {
      disallow: true
    },
    '/portfolio': {
        changefreq: 'weekly',
        priority: 0.3
    }
  }
});

sitemapRouter.get('/sitemap.xml', function(req, res) {
  sitemap.XMLtoWeb(res);
});

sitemapRouter.get('/robots.txt', function(req, res) {
  sitemap.TXTtoWeb(res);
});

module.exports = sitemapRouter;



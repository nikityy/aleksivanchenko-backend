var express = require('express');
var bodyParser = require('body-parser');
var articleAPIRouter = express.Router();

var Article = require('../models/article');

articleAPIRouter.use(bodyParser.json());

articleAPIRouter.post('/', function(req, res, next) {
  var body = req.body;

  Article.findOne({ where: { url: body.url } })
    .then(checkIfNotExists)
    .then(() => Article.create(body))
    .then((article) => res.send(getBodyWithURL(article)))
    .catch((err) => next(err));
});

articleAPIRouter.put('/:articleId', function(req, res, next) {
  var body = req.body;
  var params = req.params;
  var articleId = params.articleId;

  if (!res.locals.permissions.canEditArticles) {
    var error = new Error('404');
    next(error);
  } else if (body.id !== articleId) {
    var error = new Error('400: Ids don\'t match');
    next(error);
  }

  Article.findById(articleId)
    .then(checkIfExists)
    .then((article) => article.update(body))
    .then((article) => res.send(getBodyWithURL(article)))
    .catch((err) => next(err));
});

articleAPIRouter.delete('/:articleId', function(req, res, next) {
  var params = req.params;
  var articleId = params.articleId;

  Article.findById(articleId)
    .then(checkIfExists)
    .then((article) => article.destroy())
    .then(() => res.send(getBodyWithURL()))
    .catch((err) => next(err));
});

articleAPIRouter.use(function(err, req, res, next) {
  var errorData = err.message.split(': ');
  var code = +errorData[0];
  var message = errorData[1];

  var payload = {
    error: { code, message },
  };

  res.send(JSON.stringify(payload));
});

function checkIfExists(item) {
  if (!item) {
    throw Error('404: Not Found');
  } else {
    return item;
  }
}

function checkIfNotExists(item) {
  if (item) {
    throw Error('400: URL already exists');
  } else {
    return item;
  }
}

function getBodyWithURL(article) {
  var url = (typeof article === 'object') ? article.url : '';
  var body = { url: '/blog/' + url };
  return JSON.stringify(body);
}

module.exports = articleAPIRouter;
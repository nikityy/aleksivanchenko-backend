var Sequelize = require('sequelize');
var config = require('../config')();
var storagePath = config.database_path;

var database = new Sequelize('database', 'username', 'password', {

  host: 'localhost',
  dialect: 'sqlite',

  pool: {
    max: 5,
    min: 0,
    idle: 10000,
  },

  storage: storagePath,

});

module.exports = database;

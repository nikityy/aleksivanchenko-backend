var Sequelize = require('sequelize');
var database = require('./database');
var md = require('jstransformer-markdown-it');

var Article = database.define('article', {

  title: {
    type: Sequelize.STRING,
    allowNull: false,
  },

  url: {
    type: Sequelize.STRING,
    validate: {
      is: /^[a-z0-9\-]+$/i
    },
    allowNull: false,
  },

  thumbnail: {
    type: Sequelize.STRING,
  },

  paragraphs: {
    type: Sequelize.STRING,
    allowNull: false
  },

}, {

  getterMethods   : {
    htmlParagraphs: function()  {
      return this.paragraphs.split('\n').map((text) => md.render(text, { inline: true }));
    }
  }

});

Article.sync();

module.exports = Article;

var config = require('../config')();
var secretCookie = config.secret_cookie;

var authMiddleware = function(req, res, next) {
  var cookies = req.cookies;
  var token = cookies.token;

  if (token === secretCookie) {
    res.locals.permissions = {
      canWriteArticles: true,
      canEditArticles: true,
      canDeleteArticles: true,
      canUpdatePortfolio: true
    };
  } else {
    res.locals.permissions = {
      canWriteArticles: false,
      canEditArticles: false,
      canDeleteArticles: false,
      canUpdatePortfolio: false
    };
  }
  next();
};

module.exports = authMiddleware;

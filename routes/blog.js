var express = require('express');
var blogRouter = express.Router();

var Article = require('../models/article');
var validators = require('../utils/validators');
var checkIfExists = validators.checkIfExists;

var RECORDS_PER_PAGE = 10;

blogRouter.get('/', function(req, res, next) {
  Article.findAndCountAll({
    limit: RECORDS_PER_PAGE,
    order: 'createdAt DESC'
  })
    .then((data) => {
      res.locals.page = 0;
      res.locals.canLoadMore = RECORDS_PER_PAGE < data.count;
      return data.rows;
    })
    .then((articles) => res.render('blog/index', { articles }))
    .catch((err) => next(err));
});

blogRouter.get('/page/:pageNumber', function(req, res, next) {
  var params = req.params;
  var pageNumber = +params.pageNumber;

  if (!isFinite(pageNumber) || pageNumber <= 0) {
    var error = new Error(404);
    next(error);
  }

  Article.findAndCountAll({
    limit: RECORDS_PER_PAGE,
    offset: RECORDS_PER_PAGE * pageNumber,
    order: 'createdAt DESC'
  })
    .then((data) => {
      res.locals.page = pageNumber;
      res.locals.canLoadMore = ((pageNumber + 1) * RECORDS_PER_PAGE) < data.count;
      return data.rows;
    })
    .then((articles) => res.render('blog/index', { articles }))
    .catch((err) => next(err));
});

blogRouter.get('/new', function(req, res, next) {
  if (res.locals.permissions.canWriteArticles) {
    res.render('blog/new');
  } else {
    var error = new Error(404);
    next(error);
  }
});

blogRouter.get('/:articleURL', function(req, res, next) {
  var params = req.params;
  var articleURL = params.articleURL;

  Article.findOne({ where: { url: articleURL } })
    .then(checkIfExists)
    .then((article) => res.render('blog/item', { article }))
    .catch((err) => next(err));
});

blogRouter.get('/:articleURL/edit', function(req, res, next) {
  var params = req.params;
  var articleURL = params.articleURL;

  if (!res.locals.permissions.canEditArticles) {
    var error = new Error(404);
    next(error);
  }

  Article.findOne({ where: { url: articleURL } })
    .then(checkIfExists)
    .then((article) => res.render('blog/edit', { article }))
    .catch((err) => next(err));
});

blogRouter.get('/:articleURL/delete', function(req, res, next) {
  var params = req.params;
  var articleURL = params.articleURL;

  if (!res.locals.permissions.canEditArticles) {
    var error = new Error(404);
    next(error);
  }

  Article.findOne({ where: { url: articleURL } })
    .then(checkIfExists)
    .then((article) => article.destroy())
    .then(() => res.redirect(301, '/blog'))
    .catch((err) => next(err));
});

module.exports = blogRouter;
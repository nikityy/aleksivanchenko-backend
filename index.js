var express = require('express');
var config = require('./config')();

var app = express();

// Configure application
app.disable('x-powered-by');
app.set('view engine', 'pug');
app.set('views', config.views_path + '/routes');
app.enable('view cache');
app.locals.basedir = config.views_path;

// Configure moment
var moment = require('moment');
moment.locale('ru');
app.use(function(req, res, next) {
  res.locals.moment = moment;
  next();
});

// Pass section and subsection
app.use(function(req, res, next) {
  var path = req.path;
  var secondSlashPosition = path.indexOf('/', 1);

  if (secondSlashPosition >= 0) {
    res.locals.section = path.substring(0, secondSlashPosition);
    res.locals.subsection = path.substring(secondSlashPosition);
  } else {
    res.locals.section = path;
  }

  next();
});

// Register static routes
var static = express.static;
var oneDay = 86400000;
app.use('/static',  static(config.static_path, { maxAge: oneDay }));
app.use('/uploads', static(config.uploads_path, { maxAge: oneDay }));
app.use('/images',  static(config.images_path, { maxAge: oneDay }));

// Use sitemap
var sitemap = require('./routes/sitemap.js');
app.use('/', sitemap);

// Use cookie parser
var cookieParser = require('cookie-parser');
app.use(cookieParser());

// Register auth middleware
app.use(require('./middlewares/auth.js'));

// Register frontend routes
app.use('/',          require('./routes/index.js'));
app.use('/portfolio', require('./routes/portfolio.js'));
app.use('/blog',      require('./routes/blog.js'));
app.use('/sign-in',   require('./routes/sign-in.js'));

// Connect API routes
app.use('/api/article', require('./api/article.js'));
app.use('/api/image',   require('./api/image.js'));

// Handle errors
app.use(require('./middlewares/errors.js'));

app.listen(process.env.PORT || 3000);

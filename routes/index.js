var express = require('express');
var Promise   = require('bluebird');
var indexRouter = express.Router();

var config = require('../config')();
var Article = require('../models/article');
var Portfolio = require('../models/portfolio');
var Photo = require('../models/photo');

const LAST_ARTICLES_COUNT = 1;

indexRouter.get('/', function(req, res, next) {
  var articles = Article.findAll({
    limit: LAST_ARTICLES_COUNT,
    order: 'createdAt DESC'
  });
  var portfolioItems = Portfolio.findAll({ order: 'sort ASC' });

  Promise.props({ articles, portfolioItems, profile: config.profile })
    .then((hash) => res.render('index/index', hash))
    .catch((err) => next(err));
});

module.exports = indexRouter;

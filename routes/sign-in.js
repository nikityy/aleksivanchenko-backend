var express = require('express');
var signInRouter = express.Router();
var config = require('../config')();
var secretKey = config.secret_key;
var secretCookie = config.secret_cookie;

signInRouter.get('/:ticket', function(req, res, next) {
  var params = req.params;
  var ticket = params.ticket;

  if (ticket === secretKey) {
    res.set('Set-Cookie', 'token=' + secretCookie + '; Path=/; Max-Age=20160');
    res.send('You\'re welcome!');
  } else {
    next();
  }
});

module.exports = signInRouter;

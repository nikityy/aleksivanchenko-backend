var express = require('express');
var Portfolio = require('../models/portfolio');
var Photo = require('../models/photo');
var portfolioRouter = express.Router();

var config = require('../config')();
var validators = require('../utils/validators');
var checkIfExists = validators.checkIfExists;

var IS_UPDATE_STARTED = false;

portfolioRouter.get('/', function(req, res) {
  Portfolio.findAll({ order: 'sort ASC' })
    .then((portfolioItems) => res.render('portfolio/index', { portfolioItems }))
});

portfolioRouter.get('/update', function(req, res, next) {
  if (res.locals.permissions.canUpdatePortfolio) {
    if (IS_UPDATE_STARTED) {
      res.send('Task was already started');
    } else {
      IS_UPDATE_STARTED = true;
      Portfolio.forceFulfillFromDirectory(config.images_path)
        .then((items) => res.send('Successfully loaded ' + items.length + ' portofolio categories.'))
        .catch((err) => next(err))
        .finally(() => IS_UPDATE_STARTED = false);
    }
  } else {
    next(new Error(404));
  }
});

portfolioRouter.get('/:portfolioUrl', function(req, res, next) {
  var params = req.params;
  var portfolioUrl = params.portfolioUrl;

  Portfolio.findOne({
    where: { url: portfolioUrl },
    include: [ { model: Photo, as: 'photos' } ]
  })
    .then(checkIfExists)
    .then((portfolio) => res.render('portfolio/item', { portfolio }))
    .catch((err) => next(err));
});

module.exports = portfolioRouter;

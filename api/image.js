var express = require('express');
var bodyParser = require('body-parser');
var multer  = require('multer');
var imageAPIRouter = express.Router();

const config = require('../config')();
var UPLOAD_DIR = config.uploads_path;

var storage = multer.diskStorage({

  destination: function(req, file, cb) {
    cb(null, UPLOAD_DIR);
  },

  filename: function(req, file, cb) {
    var filename = file.originalname;
    var parts = filename.split('.');
    var extension = parts[parts.length - 1];

    cb(null, file.fieldname + '-' + Date.now() + '.' + extension);
  }

});

var upload = multer({ storage });

imageAPIRouter.post('/', upload.any('images'), function(req, res, next) {
  var files = req.files;
  var images = files.map((file) => ({ url: '/uploads/' + file.filename }));
  res.send({ images });
});

imageAPIRouter.use(function(err, req, res, next) {
  res.send({
    error: err.message
  });
});

module.exports = imageAPIRouter;

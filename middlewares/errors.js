function errorMiddleware(err, req, res, next) {
  var errorCode = +err.message;

  switch (errorCode) {
    case 404: {
      res.status(404);
      res.render('errors/404');
      break;
    }

    default: {
      res.status(500);
      res.send('500 Server Error: \n' + err.message);
      break;
    }
  }
}

module.exports = errorMiddleware;
var Sequelize = require('sequelize');
var database  = require('./database');
var Portfolio = require('./portfolio');

var Photo = database.define('photo', {

  url: {
    type: Sequelize.STRING,
    allowNull: false,
  },

  size: {
    type: Sequelize.ENUM('normal', 'single', 'book', 'album'),
    allowNull: false,
  }

});

Photo.sync();

module.exports = Photo;
